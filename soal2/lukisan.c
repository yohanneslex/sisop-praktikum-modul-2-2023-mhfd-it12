#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>
#include <signal.h>
#include <sys/stat.h>

// Function untuk membuat folder dan mengunduh gambar
int download_gambar()
{
    char nama_folder[30];
    struct tm *current_time;
    time_t t;
    int i, j;

    while (true)
    {
        t = time(NULL);
        current_time = localtime(&t);
        strftime(nama_folder, sizeof(nama_folder), "%Y-%m-%d_%H:%M:%S", current_time);

        mkdir(nama_folder, 0777);

        for (i = 1; i <= 15; i++)
        {
            char filename[50];
            sprintf(filename, "%s/gambar%d.jpg", nama_folder, i);
            char url[50];
            int size = (int)t % 1000 + 50;
            sprintf(url, "https://picsum.photos/%d", size);
            FILE *fp = fopen(filename, "wb");
            if (fp == NULL)
            {
                printf("Error saat membuat gambar %s\n", filename);
                return 1;
            }
            printf("Mengunduh gambar %s...\n", url);
            for (j = 1; j <= 3; j++)
            {
                if (j > 1)
                    sleep(5);
                char cmd[50];
                sprintf(cmd, "curl -s -o - %s >> %s", url, filename);
                FILE *curl = popen(cmd, "r");
                if (curl == NULL)
                {
                    printf("Error saat mengunduh gambar %s\n", url);
                    return 1;
                }
                pclose(curl);
            }
            fclose(fp);
            printf("Berhasil mengunduh %s\n", filename);
        }

        sleep(30);
    }
}

int main()
{
    // Proses Daemon
    pid_t pid;

    pid = fork();
    if (pid < 0)
    {
        printf("Error dalam pembuatan child process\n");
        return 1;
    }
    if (pid > 0)
    {
        printf("Daemon process dimulai dengan PID: %d\n", pid);
        return 0;
    }

    setsid();

    // Mengunduh gambar
    download_gambar();

    return 0;
}