#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/stat.h>
#include <time.h>

// Fungsi untuk melakukan unzip file
void unzip_dir(char *filename)
{
    char cmd[50];
    sprintf(cmd, "unzip -q %s", filename);
    system(cmd);
}

// Fungsi untuk membuat direktori
void create_dir(char *dirname)
{
    struct stat st = {0};
    if (stat(dirname, &st) == -1)
    {
        mkdir(dirname, 0700);
    }
}

// Fungsi untuk memindahkan file ke direktori yang sesuai
void move_file(char *filename, char *dir)
{
    char cmd[50];
    sprintf(cmd, "mv %s %s", filename, dir);
    system(cmd);
}

// Fungsi untuk melakukan zip pada direktori yang sudah dibuat
void zip_dir(char *dirname)
{
    char cmd[50];
    sprintf(cmd, "zip -r -q %s.zip %s", dirname, dirname);
    system(cmd);
}

void delete_dir(char *dirname)
{
    char cmd[50];
    sprintf(cmd, "rm -r %s", dirname);
    system(cmd);
}

// Fungsi untuk meng-generate nama file gambar secara acak
void jaga_binatang()
{
    DIR *dir = opendir(".");
    if (dir == NULL)
    {
        printf("Folder kebun_binatang tidak ditemukan\n");
        return 1;
    }

    // Simpan daftar nama file dalam array
    char nama_file[10][50];
    int num_files = 0;
    struct dirent *entry;
    while ((entry = readdir(dir)) != NULL)
    {
        if (entry->d_type == DT_REG)
        {
            snprintf(nama_file[num_files], sizeof(nama_file[num_files]), "%s", entry->d_name);
            num_files++;
        }
    }

    // Pilih file secara acak
    srand(time(NULL));
    int idx = rand() % num_files;

    // Cetak nama hewan yang dipilih
    printf("Kamu harus menjaga %s\n", nama_file[idx]);

    // Tutup folder
    closedir(dir);
}

void filter_hewan()
{
    // Membuka direktori tempat file gambar disimpan
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir(".")) != NULL)
    {
        // Membaca setiap file yang ada di direktori
        while ((ent = readdir(dir)) != NULL)
        {
            // Memeriksa apakah file adalah file gambar
            if (strstr(ent->d_name, ".jpg") != NULL)
            {
                // Menentukan tempat tinggal hewan dari nama file
                char *jenis_hewan;
                if (strstr(ent->d_name, "_darat") != NULL)
                {
                    jenis_hewan = "HewanDarat";
                }
                else if (strstr(ent->d_name, "_amphibi") != NULL)
                {
                    jenis_hewan = "HewanAmphibi";
                }
                else if (strstr(ent->d_name, "_air") != NULL)
                {
                    jenis_hewan = "HewanAir";
                }

                // Memindahkan file ke direktori yang sesuai
                move_file(ent->d_name, jenis_hewan);
            }
        }
        closedir(dir);
    }
}

int main()
{
    // Download file dari link yang diberikan
    system("wget 'https://drive.google.com/uc?export=download&id=1oDgj5kSiDO0tlyS7-20uz7t20X3atwrq' -O kebun_binatang.zip");

    // Unzip file yang telah didownload
    unzip_dir("kebun_binatang.zip");

    // Membuat direktori HewanDarat, HewanAmphibi, dan HewanAir
    create_dir("HewanDarat");
    create_dir("HewanAmphibi");
    create_dir("HewanAir");

    filter_hewan();

    // Melakukan zip pada direktori yang sudah dibuat
    zip_dir("HewanDarat");
    zip_dir("HewanAmphibi");
    zip_dir("HewanAir");

    // Menghapus direktori yang sudah dibuat
    delete_dir("HewanDarat");
    delete_dir("HewanAmphibi");
    delete_dir("HewanAir");

    // Generate nama file gambar binatang secara acak
    jaga_binatang();

    return 0;
}