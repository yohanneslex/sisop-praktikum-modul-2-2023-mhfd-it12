#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <signal.h>

volatile sig_atomic_t keep_running = 1;

void sig_handler(int signum) {
    keep_running = 0;
}

int main(int argc, char *argv[]) {
    int hours = -1, minutes = -1, seconds = -1;
    char *command = NULL;

    if (argc < 5) {
        printf("Usage: %s <hours> <minutes> <seconds> <command>\n", argv[0]);
        return 1;
    }

    hours = atoi(argv[1]);
    if (hours < 0 || hours > 23) {
        printf("Invalid value for hours.\n");
        return 1;
    }

    minutes = atoi(argv[2]);
    if (minutes < 0 || minutes > 59) {
        printf("Invalid value for minutes.\n");
        return 1;
    }

    seconds = atoi(argv[3]);
    if (seconds < 0 || seconds > 59) {
        printf("Invalid value for seconds.\n");
        return 1;
    }

    command = argv[4];

    char *tokens[3];
    tokens[0] = argv[1];
    tokens[1] = argv[2];
    tokens[2] = argv[3];

    pid_t pid, sid;

    // Fork off the parent process
    pid = fork();

    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        exit(EXIT_SUCCESS);
    }

    // Create a new SID for the child process
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }

    // Close out the standard file descriptors
    close(STDIN_FILENO);
    close(STDOUT_FILENO);
    close(STDERR_FILENO);

    // Catch the SIGTERM signal and set the flag to stop the loop
    signal(SIGTERM, sig_handler);

    // Main loop
    while (keep_running) {
        time_t now = time(NULL);
        struct tm *tm_now = localtime(&now);
        int curHours = tm_now->tm_hour;
        int curMinutes = tm_now->tm_min;
        int curSeconds = tm_now->tm_sec;

        if ((hours == -1 || hours == curHours || strcmp(tokens[0], "*") == 0) &&
            (minutes == -1 || minutes == curMinutes || strcmp(tokens[1], "*") == 0) &&
            (seconds == -1 || seconds == curSeconds || strcmp(tokens[2], "*") == 0)) {
       
            pid = fork();

            if (pid == 0) {
                char *args[] = {"/bin/bash", "-c", command, NULL};
                execvp(args[0], args);
                perror("execvp");
                exit(1);
            } else if (pid > 0) {
                waitpid(pid, NULL, 0);
            } else {
                perror("fork");
                exit(1);
            }
        }

        sleep(1);
    }

    exit(EXIT_SUCCESS);
}